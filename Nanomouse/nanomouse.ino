#include <Servo.h>

#define RIGHT 1
#define LEFT -1

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;
const byte buttonPin = 9;

const byte power = 250;

void forward()
{
  leftServo.writeMicroseconds(1500 - power);
  rightServo.writeMicroseconds(1500 + power);
}

void forwardTime(unsigned int time)
{
  forward();
  delay(time);
  stop();
}

void stop()
{
  leftServo.writeMicroseconds(1500);
  rightServo.writeMicroseconds(1500);

}

void turn(int direction, int degrees)
{
  leftServo.writeMicroseconds(1500 + power*direction);
  rightServo.writeMicroseconds(1500 + power*direction);
  if(direction==RIGHT)
   delay(degrees*5.7);
  else
   delay(degrees*5.8); 
  stop();
}
void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  leftServo.attach(6);
  rightServo.attach(5);
  
  stop();

  while (digitalRead(buttonPin))
  {

  }
  forwardTime(500);
  turn(LEFT,90);
  forwardTime(500);
  turn(RIGHT, 90);
  //forward();
  forwardTime(2000);
  //turn(500);
  //delay(1000);
  //stop();



}

void loop() {
  // put your main code here, to run repeatedly:

}